<footer class="container-fluid p-0" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">
    <div class="row no-gutters">
        <?php $social_options = get_option('dpower_social_settings'); ?>

        <div class="the-footer col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row row-footer align-items-center justify-content-between">
                    <?php if ( is_active_sidebar( 'sidebar_footer' ) ) : ?>
                    <div class="footer-item footer-item-1 col-xl col-lg col-md-4 col-sm-12 col-12">
                        <div class="social-widget">
                            <?php if (isset($social_options['facebook'])) { ?>
                            <?php if ($social_options['facebook'] != '' ) { ?>
                            <a href="<?php echo esc_url($social_options['facebook']);?>" title="<?php _e('Visíta nuestra página de Facebook', 'dpower'); ?>" target="_blank"><i class="fa fa-facebook"></i></a>
                            <?php } ?>
                            <?php } ?>

                            <?php if (isset($social_options['twitter'])) { ?>
                            <?php if ($social_options['twitter'] != '') { ?>
                            <a href="<?php echo esc_url($social_options['twitter']);?>" title="<?php _e('Visíta nuestro perfil en Twitter', 'dpower'); ?>" target="_blank"><i class="fa fa-twitter"></i></a>
                            <?php } ?>
                            <?php } ?>

                            <?php if (isset($social_options['instagram'])) { ?>
                            <?php if ($social_options['instagram'] != '') { ?>
                            <a href="<?php echo esc_url($social_options['instagram']);?>" title="<?php _e('Visita nuestro perfil en Instagram', 'dpower'); ?>" target="_blank"><i class="fa fa-instagram"></i></a>
                            <?php } ?>
                            <?php } ?>

                            <?php if (isset($social_options['youtube'])) { ?>
                            <?php if ($social_options['youtube'] != '') { ?>
                            <a href="<?php echo esc_url($social_options['youtube']);?>" title="<?php _e('Visita nuestro canal en YouTube', 'dpower'); ?>" target="_blank"><i class="fa fa-youtube"></i></a>
                            <?php } ?>
                            <?php } ?>

                            <?php if (isset($social_options['linkedin'])) { ?>
                            <?php if ($social_options['linkedin'] != '') { ?>
                            <a href="<?php echo esc_url($social_options['linkedin']);?>" title="<?php _e('Visita nuestra página en LinkedIn', 'dpower'); ?>" target="_blank"><i class="fa fa-linkedin"></i></a>
                            <?php } ?>
                            <?php } ?>

                            <?php if (isset($social_options['pinterest'])) { ?>
                            <?php if ($social_options['pinterest'] != '') { ?>
                            <a href="<?php echo esc_url($social_options['pinterest']);?>" title="<?php _e('Visita nuestra página en Pinterest', 'dpower'); ?>" target="_blank"><i class="fa fa-pinterest"></i></a>
                            <?php } ?>
                            <?php } ?>
                        </div>
                        <ul id="sidebar-footer1">
                            <?php dynamic_sidebar( 'sidebar_footer' ); ?>
                        </ul>
                    </div>
                    <?php endif; ?>

                    <?php if ( is_active_sidebar( 'sidebar_footer-2' ) ) : ?>
                    <div class="footer-item footer-item-2 col-xl col-lg col-md-4 col-sm-12 col-12">
                        <ul id="sidebar-footer2">
                            <?php dynamic_sidebar( 'sidebar_footer-2' ); ?>
                        </ul>
                    </div>
                    <?php endif; ?>

                    <?php if ( is_active_sidebar( 'sidebar_footer-3' ) ) : ?>
                    <div class="footer-item footer-item-3 col-xl col-lg col-md-4 col-sm-12 col-12">
                        <ul id="sidebar-footer3">
                            <?php dynamic_sidebar( 'sidebar_footer-3' ); ?>
                        </ul>
                    </div>
                    <?php endif; ?>
                    <div class="w-100"></div>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer() ?>
</body>

</html>
