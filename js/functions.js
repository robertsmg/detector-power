jQuery(document).ready(function ($) {
    "use strict";

    var countDownDate = new Date(jQuery('#countdown').data('countdate')).getTime();

    // Update the count down every 1 second
    var x = setInterval(function () {

        // Get today's date and time
        var now = new Date().getTime();

        // Find the distance between now and the count down date
        var distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // Display the result in the element with id="demo"
        document.getElementById("countdown").innerHTML = "<div>" + days + "<span>days</span></div>" + "<div>" + hours + "<span>hours</span></div>" + "<div>" + minutes + "<span>mins</span></div>" + "<div>" + seconds + "<span>secs</span></div>";
        // Display the result in the element with id="demo"
        document.getElementById("countdown2").innerHTML = "<div>" + days + "<span>days</span></div>" + "<div>" + hours + "<span>hours</span></div>" + "<div>" + minutes + "<span>mins</span></div>" + "<div>" + seconds + "<span>secs</span></div>";



        // If the count down is finished, write some text
        if (distance < 0) {
            clearInterval(x);
            document.getElementById("countdown").innerHTML = "EXPIRED";
            document.getElementById("countdown2").innerHTML = "EXPIRED";
        }
    }, 1000);


    jQuery('.img-close').on('click', function (e) {
        jQuery('.floating-bar').toggleClass('floating-bar-fixed');
        jQuery('.floating-bar').toggleClass('floating-bar-triggered');
    });

    jQuery(window).scroll(function (event) {
        var scroll = jQuery(window).scrollTop();
        var scrollFooter = jQuery('.the-footer').offset().top - 1050;
        if (scroll >= scrollFooter) {
            jQuery('.floating-bar').removeClass('floating-bar-fixed');
        } else {
            jQuery('.floating-bar').addClass('floating-bar-fixed');
        }

    });
    //trigger the scroll
    jQuery(window).scroll(); //ensure if you're in current position when page is refreshed

    jQuery('.the-distro-slider-container').owlCarousel({
        items: 5,
        nav: false,
        dots: false,
        autoplay: true,
        margin: 30,
        loop: true,
        responsive: {
            0: {
                items: 1
            },
            420: {
                items: 2
            },
            768: {
                items: 3
            },
            991: {
                items: 4
            },
            1170: {
                items: 5
            }
        }
    });

}); /* end of as page load scripts */
