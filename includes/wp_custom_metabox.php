<?php
function be_metabox_show_on_slug( $display, $meta_box ) {
    if ( ! isset( $meta_box['show_on']['key'], $meta_box['show_on']['value'] ) ) {
        return $display;
    }

    if ( 'slug' !== $meta_box['show_on']['key'] ) {
        return $display;
    }

    $post_id = 0;

    // If we're showing it based on ID, get the current ID
    if ( isset( $_GET['post'] ) ) {
        $post_id = $_GET['post'];
    } elseif ( isset( $_POST['post_ID'] ) ) {
        $post_id = $_POST['post_ID'];
    }

    if ( ! $post_id ) {
        return $display;
    }

    $slug = get_post( $post_id )->post_name;

    // See if there's a match
    return in_array( $slug, (array) $meta_box['show_on']['value']);
}

add_filter( 'cmb2_show_on', 'be_metabox_show_on_slug', 10, 2 );


function ed_metabox_include_front_page( $display, $meta_box ) {
    if ( ! isset( $meta_box['show_on']['key'] ) ) {
        return $display;
    }

    if ( 'front-page' !== $meta_box['show_on']['key'] ) {
        return $display;
    }

    $post_id = 0;

    // If we're showing it based on ID, get the current ID
    if ( isset( $_GET['post'] ) ) {
        $post_id = $_GET['post'];
    } elseif ( isset( $_POST['post_ID'] ) ) {
        $post_id = $_POST['post_ID'];
    }

    if ( ! $post_id ) {
        return false;
    }

    // Get ID of page set as front page, 0 if there isn't one
    $front_page = get_option( 'page_on_front' );

    // there is a front page set and we're on it!
    return $post_id == $front_page;
}
add_filter( 'cmb2_show_on', 'ed_metabox_include_front_page', 10, 2 );

add_action( 'cmb2_admin_init', 'dpower_register_custom_metabox' );
function dpower_register_custom_metabox() {
    $prefix = 'dp_';

    /* --------------------------------------------------------------
                    1.- LANDING: HERO SECTION
    -------------------------------------------------------------- */

    $cmb_landing_hero = new_cmb2_box( array(
        'id'            => $prefix . 'hero_metabox',
        'title'         => esc_html__( 'Landing: Hero', 'dpower' ),
        'object_types'  => array( 'page' ),
        'show_on'      => array( 'key' => 'slug', 'value' => array('landing') ),
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true,
        'cmb_styles' => true,
        'closed'     => false
    ) );


    $cmb_landing_hero->add_field( array(
        'id'      => $prefix . 'main_bg',
        'name'      => esc_html__( 'Hero Background', 'dpower' ),
        'desc'      => esc_html__( 'Upload a Image Background for this Hero', 'dpower' ),
        'type'    => 'file',
        'options' => array(
            'url' => false
        ),
        'text'    => array(
            'add_upload_file_text' => esc_html__( 'Upload Background', 'dpower' ),
        ),
        'query_args' => array(
            'type' => array(
                'image/gif',
                'image/jpeg',
                'image/png'
            )
        ),
        'preview_size' => 'medium'
    ) );


    $cmb_landing_hero->add_field( array(
        'id'      => $prefix . 'main_content_text',
        'name'      => esc_html__( 'Hero Second Text', 'dpower' ),
        'desc'      => esc_html__( 'Enter a descriptive text to this section', 'dpower' ),
        'type'    => 'wysiwyg',
        'options' => array(
            'textarea_rows' => get_option('default_post_edit_rows', 4),
            'teeny' => false
        )
    ) );
    /* --------------------------------------------------------------
                    2.- LANDING: DISTRIBUITORS SECTION
    -------------------------------------------------------------- */

    $cmb_landing_distro = new_cmb2_box( array(
        'id'            => $prefix . 'distro_metabox',
        'title'         => esc_html__( 'Landing: Distributors', 'dpower' ),
        'object_types'  => array( 'page' ),
        'show_on'      => array( 'key' => 'slug', 'value' => array('landing') ),
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true,
        'cmb_styles' => true,
        'closed'     => false
    ) );

    $cmb_landing_distro->add_field( array(
        'id'      => $prefix . 'distro_content_text',
        'name'      => esc_html__( 'Main Text', 'dpower' ),
        'desc'      => esc_html__( 'Enter a descriptive text to this section', 'dpower' ),
        'type'    => 'wysiwyg',
        'options' => array(
            'textarea_rows' => get_option('default_post_edit_rows', 4),
            'teeny' => false
        )
    ) );

    $cmb_landing_distro->add_field( array(
        'id'      => $prefix . 'distro_offer_text',
        'name'      => esc_html__( 'Offer Text', 'dpower' ),
        'desc'      => esc_html__( 'Enter a descriptive text to this offer', 'dpower' ),
        'type'    => 'text'
    ) );


    $group_field_id = $cmb_landing_distro->add_field( array(
        'id'          => $prefix . 'distro_group',
        'type'        => 'group',
        'description' => __( 'Benefits Section"', 'dpower' ),
        'options'     => array(
            'group_title'       => __( 'Distributor {#}', 'dpower' ),
            'add_button'        => __( 'Add Distributor', 'dpower' ),
            'remove_button'     => __( 'Remove Distributor', 'dpower' ),
            'sortable'          => true,
            'closed'         => true,
            'remove_confirm' => esc_html__( 'Are you sure to delete this element?', 'dpower' )
        )
    ) );

    $cmb_landing_distro->add_group_field( $group_field_id, array(
        'id'   => 'logo',
        'name'      => esc_html__( 'Logo', 'dpower' ),
        'desc'      => esc_html__( 'Upload an Logo for this element', 'dpower' ),
        'type'    => 'file',

        'options' => array(
            'url' => false
        ),
        'text'    => array(
            'add_upload_file_text' => esc_html__( 'Upload Logo', 'dpower' ),
        ),
        'query_args' => array(
            'type' => array(
                'image/gif',
                'image/jpeg',
                'image/png'
            )
        ),
        'preview_size' => 'avatar'
    ) );

    $cmb_landing_distro->add_group_field( $group_field_id, array(
        'id'      => 'link',
        'name'      => esc_html__( 'Distribuitor URL Link', 'dpower' ),
        'desc'      => esc_html__( 'Ingrese aquí el texto del Hero principal', 'dpower' ),
        'type'    => 'text'
    ) );

    /* --------------------------------------------------------------
                    3.- LANDING: TESTIMONIALS SECTION
    -------------------------------------------------------------- */
    $cmb_landing_testimonials = new_cmb2_box( array(
        'id'            => $prefix . 'testimonials_metabox',
        'title'         => esc_html__( 'Landing: Testimonials', 'dpower' ),
        'object_types'  => array( 'page' ),
        'show_on'      => array( 'key' => 'slug', 'value' => array('landing') ),
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true,
        'cmb_styles' => true,
        'closed'     => false
    ) );

    $cmb_landing_testimonials->add_field( array(
        'id'      => $prefix . 'testimonial_bg',
        'name'      => esc_html__( 'Testimonial Background', 'dpower' ),
        'desc'      => esc_html__( 'Upload a Image Background for this Hero', 'dpower' ),
        'type'    => 'file',
        'options' => array(
            'url' => false
        ),
        'text'    => array(
            'add_upload_file_text' => esc_html__( 'Upload Background', 'dpower' ),
        ),
        'query_args' => array(
            'type' => array(
                'image/gif',
                'image/jpeg',
                'image/png'
            )
        ),
        'preview_size' => 'medium'
    ) );


    $cmb_landing_testimonials->add_field( array(
        'id'      => $prefix . 'testimonial_content_text',
        'name'      => esc_html__( 'Main Text', 'dpower' ),
        'desc'      => esc_html__( 'Enter a descriptive text to this section', 'dpower' ),
        'type'    => 'wysiwyg',
        'options' => array(
            'textarea_rows' => get_option('default_post_edit_rows', 4),
            'teeny' => false
        )
    ) );

    $group_field_id = $cmb_landing_testimonials->add_field( array(
        'id'          => $prefix . 'testimonial_group',
        'type'        => 'group',
        'description' => __( 'Testimonials from Clients"', 'dpower' ),
        'options'     => array(
            'group_title'       => __( 'Testimonial {#}', 'dpower' ),
            'add_button'        => __( 'Add Testimonial', 'dpower' ),
            'remove_button'     => __( 'Remove Testimonial', 'dpower' ),
            'sortable'          => true,
            'closed'         => true,
            'remove_confirm' => esc_html__( 'Are you sure to delete this Testimonial?', 'dpower' )
        )
    ) );


    $cmb_landing_testimonials->add_group_field( $group_field_id, array(
        'id'      => 'name',
        'name'      => esc_html__( 'Testimonial Name', 'dpower' ),
        'desc'      => esc_html__( 'Enter a descriptive name to this section', 'dpower' ),
        'type'    => 'text'
    ) );

    $cmb_landing_testimonials->add_group_field( $group_field_id, array(
        'id'   => 'description',
        'name'      => esc_html__( 'Testimonial Text', 'dpower' ),
        'desc'      => esc_html__( 'Enter a Descriptive Text for this element', 'dpower' ),
        'type'    => 'wysiwyg',
        'options' => array(
            'textarea_rows' => get_option('default_post_edit_rows', 4),
            'teeny' => false
        )
    ) );

    /* --------------------------------------------------------------
                    4.- LANDING: PRODUCT SECTION
    -------------------------------------------------------------- */

    $cmb_landing_product = new_cmb2_box( array(
        'id'            => $prefix . 'products_metabox',
        'title'         => esc_html__( 'Landing: Product', 'dpower' ),
        'object_types'  => array( 'page' ),
        'show_on'      => array( 'key' => 'slug', 'value' => array('landing') ),
        'context'    => 'normal',
        'priority'   => 'high',
        'show_names' => true,
        'cmb_styles' => true,
        'closed'     => false
    ) );

    $cmb_landing_product->add_field( array(
        'id'      => $prefix . 'product_image',
        'name'      => esc_html__( 'Product Image', 'dpower' ),
        'desc'      => esc_html__( 'Upload a Product Image', 'dpower' ),
        'type'    => 'file',
        'options' => array(
            'url' => false
        ),
        'text'    => array(
            'add_upload_file_text' => esc_html__( 'Upload Image', 'dpower' ),
        ),
        'query_args' => array(
            'type' => array(
                'image/gif',
                'image/jpeg',
                'image/png'
            )
        ),
        'preview_size' => 'medium'
    ) );


    $cmb_landing_product->add_field( array(
        'id'      => $prefix . 'product_content_text',
        'name'      => esc_html__( 'Main Text', 'dpower' ),
        'desc'      => esc_html__( 'Enter a descriptive text to this section', 'dpower' ),
        'type'    => 'wysiwyg',
        'options' => array(
            'textarea_rows' => get_option('default_post_edit_rows', 4),
            'teeny' => false
        )
    ) );



    $cmb_landing_product->add_field( array(
        'id'      => $prefix . 'product_link',
        'name'      => esc_html__( 'Distribuitor URL Link', 'dpower' ),
        'desc'      => esc_html__( 'Enter a descriptive link to this section', 'dpower' ),
        'type'    => 'text_url'
    ) );


    $cmb_landing_product->add_field( array(
        'id'   => $prefix . 'countdown_date',
        'name'      => esc_html__( 'Valid time', 'sfwshades' ),
        'desc'      => esc_html__( 'Enter the Valid date of this coupon', 'sfwshades' ),
        'type' => 'text_date'
        // 'timezone_meta_key' => 'wiki_test_timezone',
        // 'date_format' => 'l jS \of F Y',
    ) );

    $cmb_landing_product->add_field( array(
        'id'      => $prefix . 'floating_bar_text',
        'name'      => esc_html__( 'Main Text', 'dpower' ),
        'desc'      => esc_html__( 'Enter a descriptive text to this section', 'dpower' ),
        'type'    => 'wysiwyg',
        'options' => array(
            'textarea_rows' => get_option('default_post_edit_rows', 4),
            'teeny' => false
        )
    ) );

    $cmb_landing_product->add_field( array(
        'id'      => $prefix . 'floating_bar_link',
        'name'      => esc_html__( 'Floating URL Link', 'dpower' ),
        'desc'      => esc_html__( 'Enter a descriptive link to this section', 'dpower' ),
        'type'    => 'text_url'
    ) );
}
