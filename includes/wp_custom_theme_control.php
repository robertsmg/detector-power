<?php
/* --------------------------------------------------------------
CUSTOM AREA FOR OPTIONS DATA - SANTIAGO DUARTE
-------------------------------------------------------------- */

add_action( 'customize_register', 'dpower_customize_register' );

function dpower_customize_register( $wp_customize ) {
    /* HEADER */
    $wp_customize->add_section('dpower_header_settings', array(
        'title'    => __('Cabecera', 'dpower'),
        'description' => __('Opciones para los elementos de la cabecera', 'dpower'),
        'priority' => 30
    ));

    $wp_customize->add_setting('dpower_header_settings[phone_number]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'
    ));

    $wp_customize->add_control( 'phone_number', array(
        'type' => 'text',
        'label'    => __('Número Telefónico', 'dpower'),
        'description' => __( 'Agregar número telefonico con formato para el link', 'dpower' ),
        'section'  => 'dpower_header_settings',
        'settings' => 'dpower_header_settings[phone_number]'
    ));

    $wp_customize->add_setting('dpower_header_settings[formatted_phone_number]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'
    ));

    $wp_customize->add_control( 'formatted_phone_number', array(
        'type' => 'text',
        'label'    => __('Número Telefónico [Visible]', 'dpower'),
        'description' => __( 'Agregar número telefónico en un formato visible para el público', 'dpower' ),
        'section'  => 'dpower_header_settings',
        'settings' => 'dpower_header_settings[formatted_phone_number]'
    ));

    /* SOCIAL */
    $wp_customize->add_section('dpower_social_settings', array(
        'title'    => __('Redes Sociales', 'dpower'),
        'description' => __('Agregue aqui las redes sociales de la página, serán usadas globalmente', 'dpower'),
        'priority' => 175,
    ));

    $wp_customize->add_setting('dpower_social_settings[facebook]', array(
        'default'           => '',
        'sanitize_callback' => 'dpower_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control( 'facebook', array(
        'type' => 'url',
        'section' => 'dpower_social_settings',
        'settings' => 'dpower_social_settings[facebook]',
        'label' => __( 'Facebook', 'dpower' ),
    ) );

    $wp_customize->add_setting('dpower_social_settings[twitter]', array(
        'default'           => '',
        'sanitize_callback' => 'dpower_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control( 'twitter', array(
        'type' => 'url',
        'section' => 'dpower_social_settings',
        'settings' => 'dpower_social_settings[twitter]',
        'label' => __( 'Twitter', 'dpower' ),
    ) );

    $wp_customize->add_setting('dpower_social_settings[instagram]', array(
        'default'           => '',
        'sanitize_callback' => 'dpower_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control( 'instagram', array(
        'type' => 'url',
        'section' => 'dpower_social_settings',
        'settings' => 'dpower_social_settings[instagram]',
        'label' => __( 'Instagram', 'dpower' ),
    ) );

    $wp_customize->add_setting('dpower_social_settings[linkedin]', array(
        'default'           => '',
        'sanitize_callback' => 'dpower_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control( 'linkedin', array(
        'type' => 'url',
        'section' => 'dpower_social_settings',
        'settings' => 'dpower_social_settings[linkedin]',
        'label' => __( 'LinkedIn', 'dpower' ),
    ) );

    $wp_customize->add_setting('dpower_social_settings[youtube]', array(
        'default'           => '',
        'sanitize_callback' => 'dpower_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control( 'youtube', array(
        'type' => 'url',
        'section' => 'dpower_social_settings',
        'settings' => 'dpower_social_settings[youtube]',
        'label' => __( 'YouTube', 'dpower' ),
    ) );

    $wp_customize->add_setting('dpower_social_settings[pinterest]', array(
        'default'           => '',
        'sanitize_callback' => 'dpower_sanitize_url',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control( 'pinterest', array(
        'type' => 'url',
        'section' => 'dpower_social_settings',
        'settings' => 'dpower_social_settings[pinterest]',
        'label' => __( 'Pinterest', 'dpower' ),
    ) );


    $wp_customize->add_section('dpower_cookie_settings', array(
        'title'    => __('Cookies', 'dpower'),
        'description' => __('Opciones de Cookies', 'dpower'),
        'priority' => 176,
    ));

    $wp_customize->add_setting('dpower_cookie_settings[cookie_text]', array(
        'default'           => '',
        'sanitize_callback' => 'sanitize_text_field',
        'capability'        => 'edit_theme_options',
        'type'           => 'option'

    ));

    $wp_customize->add_control( 'cookie_text', array(
        'type' => 'textarea',
        'label'    => __('Cookie consent', 'dpower'),
        'description' => __( 'Texto del Cookie consent.' ),
        'section'  => 'dpower_cookie_settings',
        'settings' => 'dpower_cookie_settings[cookie_text]'
    ));

    $wp_customize->add_setting('dpower_cookie_settings[cookie_link]', array(
        'default'           => '',
        'sanitize_callback' => 'absint',
        'capability'        => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control( 'cookie_link', array(
        'type'     => 'dropdown-pages',
        'section' => 'dpower_cookie_settings',
        'settings' => 'dpower_cookie_settings[cookie_link]',
        'label' => __( 'Link de Cookies', 'dpower' ),
    ) );

}

function dpower_sanitize_url( $url ) {
    return esc_url_raw( $url );
}
