<?php

function portafolio() {

    $labels = array(
        'name'                  => _x( 'Portafolios', 'Post Type General Name', 'dpower' ),
        'singular_name'         => _x( 'Portafolio', 'Post Type Singular Name', 'dpower' ),
        'menu_name'             => __( 'Portafolio', 'dpower' ),
        'name_admin_bar'        => __( 'Portafolio', 'dpower' ),
        'archives'              => __( 'Archivo de Portafolio', 'dpower' ),
        'attributes'            => __( 'Atributos de Portafolio', 'dpower' ),
        'parent_item_colon'     => __( 'Portafolio Padre:', 'dpower' ),
        'all_items'             => __( 'Todos los Items', 'dpower' ),
        'add_new_item'          => __( 'Agregar Nuevo Item', 'dpower' ),
        'add_new'               => __( 'Agregar Nuevo', 'dpower' ),
        'new_item'              => __( 'Nuevo Item', 'dpower' ),
        'edit_item'             => __( 'Editar Item', 'dpower' ),
        'update_item'           => __( 'Actualizar Item', 'dpower' ),
        'view_item'             => __( 'Ver Item', 'dpower' ),
        'view_items'            => __( 'Ver Portafolio', 'dpower' ),
        'search_items'          => __( 'Buscar en Portafolio', 'dpower' ),
        'not_found'             => __( 'No hay Resultados', 'dpower' ),
        'not_found_in_trash'    => __( 'No hay Resultados en la Papelera', 'dpower' ),
        'featured_image'        => __( 'Imagen Destacada', 'dpower' ),
        'set_featured_image'    => __( 'Colocar Imagen Destacada', 'dpower' ),
        'remove_featured_image' => __( 'Remover Imagen Destacada', 'dpower' ),
        'use_featured_image'    => __( 'Usar como Imagen Destacada', 'dpower' ),
        'insert_into_item'      => __( 'Insertar dentro de Item', 'dpower' ),
        'uploaded_to_this_item' => __( 'Cargado a este item', 'dpower' ),
        'items_list'            => __( 'Listado del Portafolio', 'dpower' ),
        'items_list_navigation' => __( 'Navegación de Listado del Portafolio', 'dpower' ),
        'filter_items_list'     => __( 'Filtro de Listado del Portafolio', 'dpower' ),
    );
    $args = array(
        'label'                 => __( 'Portafolio', 'dpower' ),
        'description'           => __( 'Portafolio de Desarrollos', 'dpower' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', 'comments', 'trackbacks', 'custom-fields', ),
        'taxonomies'            => array( 'custom_portafolio' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-testimonial',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
        'show_in_rest'          => true,
    );
    register_post_type( 'portafolio', $args );

}
add_action( 'init', 'portafolio', 0 );
