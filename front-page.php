<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid p-0" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row no-gutters">
        <?php /* HERO CONTAINER */ ?>
        <?php $bg_hero_id = get_post_meta(get_the_ID(), 'dp_main_bg_id', true); ?>
        <?php $bg_hero = wp_get_attachment_image_src($bg_hero_id, 'full', false); ?>
        <section class="the-hero col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="background: url(<?php echo $bg_hero[0]; ?>);">
            <div class="container">
                <div class="row">
                    <div class="hero-text-content col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <?php the_content(); ?>
                        <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'dp_main_content_text', true)); ?>
                    </div>
                </div>
            </div>
        </section>

        <section class="the-distro-info col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row">
                    <div class="distro-info-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'dp_distro_content_text', true)); ?>
                        <div class="offer-text">
                            <?php echo get_post_meta(get_the_ID(), 'dp_distro_offer_text', true); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="the-distro-slider col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="distro-slider-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="the-distro-slider-container owl-carousel owl-theme">
                    <?php $distro_group = get_post_meta(get_the_ID(), 'dp_distro_group', true); ?>
                    <?php if (!empty($distro_group)) { ?>
                    <?php foreach ($distro_group as $distro_item) { ?>
                    <div class="distro-item item">
                        <img src="<?php echo $distro_item['logo']; ?>" alt="Distribuitor" class="img-fluid" />
                        <a href="<?php echo $distro_item['link']; ?>" class="btn btn-md btn-distro"><?php _e('View Products', 'dpower'); ?></a>
                    </div>
                    <?php } ?>
                    <?php } ?>
                </div>
            </div>
        </section>
        <?php $bg_hero_id = get_post_meta(get_the_ID(), 'dp_testimonial_bg_id', true); ?>
        <?php $bg_hero = wp_get_attachment_image_src($bg_hero_id, 'full', false); ?>
        <section class="the-testimonials col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="background: url(<?php echo $bg_hero[0]; ?>);">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="testimonials-content col-xl-10 col-lg-10 col-md-11 col-sm-12 col-12">
                        <div class="testimonial-intro">
                            <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'dp_testimonial_content_text', true)); ?>
                        </div>
                        <div class="testimonial-boxed-content col-12">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/quote-vector.png" alt="Quotes" class="img-fluid img-quote" />
                            <?php $testimonials_group = get_post_meta(get_the_ID(), 'dp_testimonial_group', true); ?>
                            <?php if (!empty($testimonials_group)) { ?>
                            <?php foreach ($testimonials_group as $testimonials_item) { ?>
                            <div class="testimonial-item item">
                                <div class="testimonials-stars">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                                <?php echo apply_filters('the_content', $testimonials_item['description']); ?>
                                <h4 class="testimonial-item-name"><?php echo $testimonials_item['name']; ?></h4>
                            </div>
                            <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="the-product col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row align-items-center">
                    <div class="product-pic col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 d-xl-none d-lg-none d-md-none d-sm-block d-block">
                        <?php $bg_hero_id = get_post_meta(get_the_ID(), 'dp_product_image_id', true); ?>
                        <?php $bg_hero = wp_get_attachment_image_src($bg_hero_id, 'full', false); ?>
                        <img src="<?php echo $bg_hero[0]; ?>" alt="product" class="img-fluid" />
                    </div>
                    <div class="product-info col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'dp_product_content_text', true)); ?>
                    </div>
                    <div class="product-pic col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 d-xl-block d-lg-block d-md-block d-sm-none d-none">
                        <?php $bg_hero_id = get_post_meta(get_the_ID(), 'dp_product_image_id', true); ?>
                        <?php $bg_hero = wp_get_attachment_image_src($bg_hero_id, 'full', false); ?>
                        <img src="<?php echo $bg_hero[0]; ?>" alt="product" class="img-fluid" />
                    </div>
                </div>
                <div class="row">
                    <div class="product-countdown col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div data-countdate="<?php echo get_post_meta(get_the_ID(), 'dp_countdown_date', true); ?>" id="countdown" class="countdown-container">

                        </div>
                    </div>
                    <div class="product-link col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <a href="<?php echo get_post_meta(get_the_ID(), 'dp_product_link', true); ?>" class="btn btn-md btn-product"><?php _e('View All', 'dpower'); ?></a>
                    </div>
                </div>
            </div>
        </section>
        <section class="floating-bar floating-bar-fixed col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="floating-close">
                <img src="<?php echo get_template_directory_uri(); ?>/images/close-vector.png" alt="Close" class="img-fluid img-close" />
            </div>
            <div class="container">
                <div class="row align-items-center">
                    <div class="floating-bar-info col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'dp_floating_bar_text', true)); ?>
                    </div>
                    <div class="floating-bar-countdown col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                        <div data-countdate="<?php echo get_post_meta(get_the_ID(), 'dp_countdown_date', true); ?>" id="countdown2" class="countdown-container">

                        </div>
                    </div>
                    <div class="floating-bar-link col-xl-3 col-lg-3 col-md-3 col-sm-12 col-12">
                        <a href="<?php echo get_post_meta(get_the_ID(), 'dp_product_link', true); ?>" class="btn btn-md btn-product"><?php _e('Buy now!', 'dpower'); ?></a>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>
